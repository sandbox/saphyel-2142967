<?php

/**
 * @file
 * Contains \Drupal\diablo\Plugin\Block\DiabloBlock.
 */

namespace Drupal\diablo\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'Diablo block' block.
 *
 * @Block(
 *   id = "diablo_block",
 *   subject = @Translation("Diablo ID"),
 *   admin_label = @Translation("Diablo ID")
 * )
 */
class DiabloBlock extends BlockBase {
  public function build() {
    return drupal_get_form('Drupal\diablo\Form\DiabloBlockForm');
  }
}
