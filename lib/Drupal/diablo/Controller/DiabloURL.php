<?php

namespace Drupal\diablo\Controller;

use Drupal\blizzard\Controller\BnetURL;

class DiabloURL extends BnetURL {
  
  protected $battletag = '';
  
  public function __construct($region, $battletag) {
    parent::__construct($region);
    $this->setApiPath($this->apiPath . '/d3/profile/');
    $this->setBattletag($battletag);
  }
  
  public function getBattletag() {
    return $this->battletag;
  }
  
  public function getURL() {
    $url = str_replace('/api/d3/','/d3/en/',$this->url);
    return $url;
  }
  
  public function setBattletag($battletag) {
    if (empty($battletag)) {
      throw new InvalidArgumentException('Battletag cannot be empty');
    }
    
    $this->battletag = $battletag;
    $this->setApiPath($this->apiPath . '{battletag}/');
    $this->addPlaceholder('{battletag}', $battletag);
  }
  
}

