<?php

namespace Drupal\diablo\Controller;

class DiabloURLHero extends DiabloURL {
  
  protected $hero = '';
  
  public function __construct($region, $battletag, $hero) {
    parent::__construct($region, $battletag);
    $this->setApiPath($this->apiPath . 'hero/{hero}');
    $this->setHero($hero);
  }
  
  public function setHero($hero) {
    if (empty($hero)) {
      throw new InvalidArgumentException('Hero cannot be empty');
    }
    
    $this->hero = $hero;
    $this->addPlaceholder('{hero}', $hero);
  }
  
  public function renameStats() {
    switch ($this->fields['class']) {
      case 'barbarian':
        $statkey = t('fury');
        self::destroyStat('dexterity');
        self::destroyStat('intelligence');
        break;
      case 'monk':
        $statkey = t('spirit');
        self::destroyStat('strength');
        self::destroyStat('intelligence');
        break;
      case 'wizard':
        $statkey = t('arcane power');
        self::destroyStat('dexterity');
        self::destroyStat('strength');
        break;
      case 'demon-hunter':
        $statkey = t('hatred');
        $this->fields['stats'][t('discipline')] = $this->fields['stats']['secondaryResource'];
        self::destroyStat('strength');
        self::destroyStat('intelligence');
        break;
      case 'witch-doctor':
        $statkey = t('mana');
        self::destroyStat('dexterity');
        self::destroyStat('strength');
        break;
      case 'crusader':
        $statkey = t('wrath');
        self::destroyStat('dexterity');
        self::destroyStat('intelligence');
        break;
    }
    $this->fields['stats'][$statkey] = $this->fields['stats']['primaryResource'];
    self::destroyStat('primaryResource');
    self::destroyStat('secondaryResource');
    self::destroyStat('physicalResist');
    self::destroyStat('fireResist');
    self::destroyStat('coldResist');
    self::destroyStat('lightningResist');
    self::destroyStat('poisonResist');
    self::destroyStat('arcaneResist');
    self::destroyStat('magicFind');
  }
  private function destroyStat($stat) {
    unset($this->fields['stats'][$stat]);
  }

}