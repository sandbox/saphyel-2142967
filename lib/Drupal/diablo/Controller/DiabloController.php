<?php

/**
 * @file
 * Contains \Drupal\diablo\Controller\DiabloController.
 */

namespace Drupal\diablo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class DiabloController extends ControllerBase {

  /**
   * Constructs a start page with descriptive content.
   *
   * Our router maps this method to the path 'diablo'.
   */
  function home() {
    $build = array(
      '#markup' => t('<p>This Diablo 3 module is intended to be a "KISS" module, whose want learn how to develop in Drupal 8 and use JSON, this might be your favorite module!</p>'),
      drupal_get_form('Drupal\diablo\Form\DiabloBlockForm'),
      '#theme_wrappers' => array(
          'container' => array(
            '#attributes' => array(
              'class' => 'diablo-front',
            ),
          ),
        ),
    );
    drupal_add_css('modules/diablo/css/diablo.module.css');
    return $build;
  }
  /**
   * Constructs a page with profiles of Diablo.
   *
   * Our router maps this method to the path 'diablo/{host}/{battletag}'.
   */
  function profile($host, $battletag) {
    //http://eu.battle.net/api/d3/profile/Lxir-2890/
    $profile = new DiabloURL($host, $battletag);
    try {
      $profile->sendRequest();
    }
    catch (DiabloException $e) {
      drupal_set_message('Unable to send API request.', 'error');
      return FALSE;
    }
    $fields=$profile->getFields();
    
    foreach($fields['heroes'] as $herovalue) {
      $heroes_list[] = array(
        '#type' => 'link',
        '#title' => '(' . $herovalue['level'] . ') ' . t($herovalue['name']),
        '#href' => url('diablo/' . $host . '/' . $battletag . '/' . $herovalue['id'], array('absolute' => TRUE)),
        '#attributes' => array(
          'class' => $herovalue['class'] . '-' . $herovalue['gender'],
        ),
      );
    }
    $time = array(
      date("Y-m-d", $fields['lastUpdated']),
      date("d-m-Y", $fields['lastUpdated']),
    );
    $render_array = array(
      '#title' => $fields['battleTag'],
      'diablo_profile' => array(
        '#theme' => 'table',
        '#rows' => array(
          array(
            array(
              'header' => TRUE,
              'data' => 'Monsters Kills',
            ),
            array(
              'data' => $fields['kills']['monsters'],
            ),
          ),
          array(
            array(
              'header' => TRUE,
              'data' =>  'Elites Kills',
            ),
            array(
              'data' => $fields['kills']['elites'],
            ),
          ),
          array(
            array(
              'header' => TRUE,
              'data' =>  'Last update',
            ),
            array(
              'data' => '<time datetime="' . $time[0] . '">' . $time[1] . '</time>',
            ),
          ),
          array(
            array(
              'header' => TRUE,
              'data' =>  'Profile',
            ),
            array(
              'data' => '<a href="' . $profile->getURL() . '">Link Battle.net</a>',
            ),
          ),
        ),
        '#attributes' => array(
          'class' => 'info',
        ),
      ),
      'diablo_heroes' => array(
        '#theme' => 'item_list',
        '#items' => $heroes_list,
        '#title' => t('Heroes'),
        '#attributes' => array(
          'class' => 'heroes-list',
        ),
      ),
    );
    
    drupal_add_css('modules/diablo/css/diablo.module.css');
    
    return $render_array;
    
  }
  /**
   * Constructs a page with basic info about a hero.
   * Stats Table, Gear Ul
   * Our router maps this method to the path 'diablo/{host}/{battletag}/{hero}'.
   */
  function hero($host, $battletag, $hero) {
    //http://eu.battle.net/api/d3/profile/Lxir-2890/hero/11110743
    
    $char = new DiabloURLHero($host, $battletag, $hero);
    try {
      $char->sendRequest();
    }
    catch (DiabloException $e) {
      drupal_set_message('Unable to send API request.', 'error');
      return FALSE;
    }
    
    $char->renameStats();
    $fields=$char->getFields();
    
    foreach($fields['stats'] as $statkey => $statvalue) {
      
      $array_stats[] = array(
        array(
          'header' => TRUE,
          'data' => t($statkey),
          'class' => $statkey,
        ),
        array(
          'data' => $statvalue,
          'class' => $statkey,
        ),
      );
    }
    
    foreach($fields['items'] as $itemkey => $itemvalue) {
      if (!empty($itemvalue)) {
        $array_items[] = array(
          '#theme' => 'item_list',
          '#title' => t($itemkey),
          '#attributes' => array(
            'class' => 'gear-list ' . $itemkey,
          ),
          '#items' => array(
            '0' => t('Equipped:') . ' <a href="http://' . $host . '.battle.net/d3/en/' . $itemvalue['tooltipParams'] . '" class="' . $itemvalue['displayColor'] . '" data-d3tooltip="">' . $itemvalue['name'] . '</a>',
          ),
        );
      }
    }
    
    foreach($fields['skills']['active'] as $skillvalue) {
      if (!empty($skillvalue)) {
        $array_skillsa[] = array(
          '#type' => 'link',
          '#title' => $skillvalue['skill']['name'] . ': ' . $skillvalue['rune']['name'],
          '#href' => url('http://' . $host . '.battle.net/d3/en/class/' . $fields['class'] . '/active/' . $skillvalue['skill']['slug'], array('absolute' => TRUE)),
        );
      }
    }
    
    foreach($fields['skills']['passive'] as $skillvalue) {
      if (!empty($skillvalue)) {
        $array_skillsp[] = array(
          '#type' => 'link',
          '#title' => $skillvalue['skill']['name'],
          '#href' => url('http://' . $host . '.battle.net/d3/en/class/' . $fields['class'] . '/passive/' . $skillvalue['skill']['slug'], array('absolute' => TRUE)),
        );
      }
    }
    
    $render_array = array(
      '#title' => $fields['name'],
      '#id' => 'heroes',
      '#theme_wrappers' => array(
        'container' => array(
          '#attributes' => array(
            'class' => 'heroes ' . $fields['class'] . "-" . $fields['gender'],
          ),
        ),
      ),
      '#markup' => '<h4> ' . t('Level') . ' ' . $fields['level'] . ' (' .
      $fields['paragonLevel'] . ')</h4><h3>' . $fields['name'] . '</h3><script src="http://eu.battle.net/d3/static/js/tooltips.js"></script>
      <script>
var b = Bnet.D3.Tooltips;
b.registerDataOld = b.registerData;
b.registerData = function(data) {
var c = document.body.children, s = c[c.length-1].src;
data.params.key=s.substr(0,s.indexOf("?")).substr(s.lastIndexOf("/")+1);
this.registerDataOld(data);
}
</script>',
      'diablo_stats' => array(
        '#theme' => 'table',
        '#rows' => $array_stats,
        '#caption' => t('Attributes'),
        '#attributes' => array(
          'class' => 'stats-table clearfix',
        ),
      ),
      'diablo_skills'  => array(
        '#prefix' => '<div class="diablo-skills clearfix">',
        'diablo_skillsA' => array(
          '#theme' => 'item_list',
          '#title' => t("skills Active"),
          '#attributes' => array(
            'class' => 'skill-list',
          ),
          '#items' => $array_skillsa,
        ),
        'diablo_skillsP' => array(
          '#theme' => 'item_list',
          '#title' => t("skills Passive"),
          '#attributes' => array(
            'class' => 'skill-list',
          ),
          '#items' => $array_skillsp,
        ),
        '#suffix' => '</div>',
      ),
      'diablo_gear' => array(
        '#prefix' => '<div class="diablo-gear clearfix">',
        $array_items,
        '#suffix' => '</div>',
      ),
    );
    
    drupal_add_css('modules/diablo/css/diablo.module.css');
    return $render_array;

  }

}