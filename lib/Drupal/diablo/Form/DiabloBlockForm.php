<?php

/**
 * @file
 * Contains \Drupal\diablo\Form\DiabloBlockForm.
 */

namespace Drupal\diablo\Form;

use Drupal\Core\Form\FormBase;

/**
 * Builds the search form for the search block.
 */
class DiabloBlockForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'diablo_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form['diablo_host'] = array(
      '#type' => 'select',
      '#title' => t('Server'),
      '#options' => array(
        "eu" => t('Europe'),
        "us" => t('America'),
      ),
      //'#default' => 1,
      '#required' => TRUE,
      
    );
    $form['diablo_career'] = array(
      '#type' => 'textfield',
      '#title' => t('BattleTag'),
      '#description' => t('Use "-" instead of "#".'),
      '#placeholder' => 'Saphyel-2216',
      '#size' => 20,
      '#required' => TRUE,
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => $this->t('Load'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $form_state['redirect_route'] = array(
      'route_name' => 'diablo_career',
      'route_parameters' => array(
        'host' => $form_state['values']['diablo_host'],
        'battletag' => $form_state['values']['diablo_career'],
      )
    );
  }
}
